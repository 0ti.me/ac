You will need to already have installed PlayOnLinux and wine.  You could probably work around needing PlayOnLinux if you want.

Clone me to ${HOME}/.PlayOnLinux/wineprefix/ac

    git clone git@gitlab.com:0ti.me/ac ${HOME}/.PlayOnLinux/wineprefix/ac

1. Open PlayOnLinux
1. Click Configure
1. Click ac on the left
1. Make a new shortcut from this virtual drive
1. Shortcut to ThwargLauncher.exe
1. Run that shortcut

You should launch AC and
